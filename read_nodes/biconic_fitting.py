import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import fsolve
from sympy import solve, solveset, var
import sympy as sp
from scipy.io import loadmat


def biconic_fitting(data):
    x = np.reshape(data[:, 0], [len(data[:, 0]), 1])
    y = np.reshape(data[:, 1], [len(data[:, 0]), 1])
    z = np.reshape(data[:, 2], [len(data[:, 0]), 1])
    X = np.zeros([len(x), len(x)+3])
    # create Matrix for least square minimization
    for i in range(len(x)):
        X[i, 0:3] = [x[i, 0]**2, y[i, 0]**2, x[i, 0]*y[i, 0]]
        X[i, i+3] = z[i, 0]**2

    X_inv = np.linalg.pinv(X)
    p_prime = 2*np.dot(X_inv, z)
    term = np.zeros([len(x), 1])
    # create Matrix for least square minimization
    for i in range(len(x)):
        term[i, 0] = p_prime[i+3, 0]*(2*z[i, 0] - p_prime[i+3, 0]*z[i, 0]**2)

    def parameter_solve(init, *par):
        p_prime = np.reshape(par, [3, 1])
        a = init[0]
        b = init[1]
        phi = init[2]
        f_1 = a**2 + a*(-p_prime[0, 0]-p_prime[1, 0]) + (p_prime[0, 0]*p_prime[1, 0] - p_prime[2, 0]**2/4)
        f_2 = -a - b + (p_prime[0, 0] + p_prime[1, 0])
        f_3 = phi - 0.5*np.arcsin(p_prime[2, 0]/(b - a))
        f = [f_1, f_2, f_3]
        return f
    p = -np.ones([3, 1])
    i = 1
    while (p[0] <= 0 or (p_prime[0, 0] - p[0])/(p_prime[0, 0]+p_prime[1, 0] - 2*p[0]) < 0) and i < 100:
        init = np.random.rand(3, 1)*10
        init[2] = np.random.randn(1, 1)*2*np.pi
        p = fsolve(parameter_solve, init, p_prime[0:3, 0])
        i += 1

    p_prime_2 = np.dot(np.linalg.pinv(X[:, 0:3]), term)
    R_x = 1/p[0]
    R_y = 1/p[1]
    Q_x = R_x**2*(p_prime_2[0] - 0.5*p_prime_2[2]*np.tan(p[2])) - 1
    Q_y = R_y**2*(p_prime_2[1] - 0.5*p_prime_2[2]*np.tan(p[2])) - 1
    phi = p[2]
    return R_x, R_y, Q_x, Q_y, phi
