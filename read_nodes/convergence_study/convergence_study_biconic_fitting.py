import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import fsolve
from mpl_toolkits.mplot3d import Axes3D
from my_functions import *

nodes_pos_file = {'fine': 'anterior_surface_node_pos_fine.dat'} #, 'coarse_1': 'anterior_surface_node_pos_coarse.dat', 'coarse_2': 'anterior_surface_node_pos_coarse_2.dat'}
nodes_id_file = {'fine': 'anterior_surface_node_id_fine.dat'} #, 'coarse_1': 'anterior_surface_node_id_coarse.dat', 'coarse_2': 'anterior_surface_node_id_coarse_2.dat'}
nodes_disp_file = {'fine': 'anterior_surface_displacement_fine.dat'} # , 'coarse_1': 'anterior_surface_displacement_coarse.dat', 'coarse_2': 'anterior_surface_displacement_coarse_2.dat'}
plt.figure()
for f_num in nodes_disp_file:

    # load position of nodes
    nodes = []
    with open(nodes_pos_file[f_num]) as fh:
        next(fh)
        for line in fh:
            id_1 = line.find("<node id=")
            id_2 = line.find("> ")
            id_3 = line.find("</node>")
            nodes.append([int(line[id_1+10:id_2-1]), float(line[id_2+3:id_2+16]), float(line[id_2+19:id_2+32]), float(line[id_2+35:id_3])])

    # load set definition "id of nodes" or "indexes of set"
    nodes_index = []
    with open(nodes_id_file[f_num]) as fh:
        next(fh)
        for line in fh:
            id_1 = line.find("<node id=")
            id_2 = line.find("/>")
            nodes_index.append(int(line[id_1 + 10:id_2 - 1]))

    # load displacement of nodes
    nodes_disp = []
    t = []
    with open(nodes_disp_file[f_num]) as fh:
        for line in fh:
            if line.find('*Step') == 0:
                line = next(fh)
                id_1 = line.find('=')
                t.append(float(line[id_1+1:-1]))
                line = next(fh)
                line = next(fh)
            nodes_disp.append([float(line.split(',')[0]), float(line.split(',')[1]), float(line.split(',')[2]), float(line.split(',')[3])])

    nodes = np.asarray(nodes)
    nodes_index = np.asarray(nodes_index)
    nodes_disp = np.reshape(np.asarray(nodes_disp), (len(t), len(nodes_index)*4), order='C')
    t = np.asarray(t)
    x = np.zeros((len(nodes_index), 3*len(t)))
    iii = 0

    # stitch disp and pos together
    for steps in t:
        ii = 0
        for i in nodes_index:
            temp = nodes[np.where(nodes[:, 0] == i)[0], :] + nodes_disp[iii, int(np.where(nodes_disp[iii, :] == i)[0]): int(np.where(nodes_disp[iii, :] == i)[0])+4]
            x[ii, iii*3:(iii+1)*3] = temp[0, 1:]
            ii += 1
        iii += 1
    x = np.array(sorted(x, key=lambda x_column: x_column[0]))
    rot_angle = 5/180*np.pi
    slices = int(2*np.pi/rot_angle)
    skip = 1                                                            # at least one
    index_15 = (np.abs(x[:, 0] - 1.5)).argmin()
    x_revolved = np.zeros([np.int(np.ceil(index_15/skip))*slices, 3*len(t)])
    for jj in range(slices):
        R_y = np.matrix([[np.round(np.cos(jj*rot_angle), decimals=6), 0, np.round(np.sin(jj*rot_angle), decimals=6)], [0, 1, 0], [-np.round(np.sin(jj*rot_angle), decimals=6), 0, np.round(np.cos(jj*rot_angle), decimals=6)]])
        for j in range(len(t)):
            # biconic fitting describes surface in function of z; turn coordinate system accordingly
            temp = np.transpose(np.dot(R_y, np.transpose(x[0:index_15:skip, j*3:(j+1)*3])))
            x_revolved[jj*np.int(np.ceil(index_15/skip)):(jj+1)*np.int(np.ceil(index_15/skip)), j*3:(j+1)*3] = np.concatenate([temp[:, 2], temp[:, 0], temp[:, 1]], axis=1)

    # calculate Radius and power of the eye
    n = 1.3375
    R_n = np.zeros(np.array([len(t), 1]))
    R_ny = np.zeros(np.array([len(t), 1]))
    power_eye = np.zeros(np.array([len(t), 1]))

    for j in range(int(np.float(len(t)))):
        pos = x_revolved[:, j*3:(j+1)*3]
        r = sphere_fit(pos)
        # r = nm_biconic_fit(pos)
        R_n[j] = r[0]
        R_ny[j] = r[1]
        power_eye[j] = (n-1)/(R_n[j]*1e-3)

    plt.plot(t/3600, R_n, label=f_num)
    plt.legend()
    plt.xlabel('time [h]')
    plt.ylabel('radius [mm]')
    plt.xticks((np.arange(0, 22, 2)))
plt.show()
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x_revolved[:, 0*7], x_revolved[:, 1*7], x_revolved[:, 2*7])
ax.set_xlim3d(-1.5, 1.5)
ax.set_ylim3d(-1.5, 1.5)
ax.set_zlim3d(7.2, 7.8)
ax.set_xlabel('x-axis [mm]')
ax.set_ylabel('y-axis [mm]')
ax.set_zlabel('z-axis [mm]')
plt.show()