import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.odr as odr
import scipy.optimize as optimize
from sympy import solve, solveset, var
import sympy as sp
from scipy.io import loadmat


def load_feb_file_nodes(filename, section):
    nodes = []
    with open(filename) as fh:
        line = next(fh)
        while line.find(section) == -1:
            line = next(fh)
        for line in fh:
            if not line.find('</Nodes>') == -1:
                break
            id_1 = line.find("<node id=")
            id_2 = line.find("> ")
            id_3 = line.find("</node>")
            nodes.append([int(line[id_1 + 10:id_2 - 1]), float(line[id_2 + 3:id_2 + 16]), float(line[id_2 + 19:id_2 + 32]), float(line[id_2 + 35:id_3])])
    return nodes


def load_feb_file_nodes_id(filename, section):
    nodes_index = []
    with open(filename) as fh:
        line = next(fh)
        while line.find(section) == -1:
            line = next(fh)
        for line in fh:
            if not line.find('</NodeSet>') == -1:
                break
            id_1 = line.find("<node id=")
            id_2 = line.find("/>")
            nodes_index.append(int(line[id_1 + 10:id_2 - 1]))
    return nodes_index


def load_output_dat_file(filename):
    nodes_disp = []
    t = []
    with open(filename) as fh:
        for line in fh:
            if line.find('*Step') == 0:
                line = next(fh)
                id_1 = line.find('=')
                t.append(float(line[id_1 + 1:-1]))
                line = next(fh)
                line = next(fh)
            nodes_disp.append([float(line.split(',')[0]), float(line.split(',')[1]), float(line.split(',')[2]), float(line.split(',')[3])])
    return t, nodes_disp

def biconic_fitting(data):
    x = np.reshape(data[:, 0], [len(data[:, 0]), 1])
    y = np.reshape(data[:, 1], [len(data[:, 0]), 1])
    z = np.reshape(data[:, 2], [len(data[:, 0]), 1])
    X = np.zeros([len(x), len(x)+3])
    # create Matrix for least square minimization
    for i in range(len(x)):
        X[i, 0:3] = [x[i, 0]**2, y[i, 0]**2, x[i, 0]*y[i, 0]]
        X[i, i+3] = z[i, 0]**2
    p_prime = np.linalg.lstsq(X, 2*z, rcond=-1)
    p_prime = p_prime[0]
    # X_inv = np.linalg.pinv(X)
    # p_prime = 2*np.dot(X_inv, z)
    term = np.zeros([len(x), 1])
    # create Matrix for least square minimization
    for i in range(len(x)):
        term[i, 0] = p_prime[i+3, 0]*(2*z[i, 0] - p_prime[i+3, 0]*z[i, 0]**2)
    p = -np.ones([3, 1])
    a_1 = 0.5*(-(-p_prime[0, 0]-p_prime[1, 0]) + np.sqrt((-p_prime[0, 0]-p_prime[1, 0])**2 - 4*(p_prime[0, 0]*p_prime[1, 0] - p_prime[2, 0]**2/4) + 0j))
    a_2 = 0.5*(-(-p_prime[0, 0]-p_prime[1, 0]) - np.sqrt((-p_prime[0, 0]-p_prime[1, 0])**2 - 4*(p_prime[0, 0]*p_prime[1, 0] - p_prime[2, 0]**2/4) + 0j))
    a_1 = np.round(a_1, decimals=5)
    a_2 = np.round(a_2, decimals=5)
    if a_1 > 0 and (p_prime[0, 0] - a_1)/(p_prime[0, 0]+p_prime[1, 0] - 2*a_1) >= 0:
        p[0] = np.real(a_1)
    elif a_2 > 0 and (p_prime[0, 0] - a_2)/(p_prime[0, 0]+p_prime[1, 0] - 2*a_2) >= 0:
        p[0] = np.real(a_2)
    else:
        p[0] = np.inf
    p[1] = -p[0] + (p_prime[0, 0] + p_prime[1, 0])
    if p[0] == p[1]:
        p[2] = 0
    else:
        p[2] = 0.5*(np.arcsin(p_prime[2, 0]/(p[1] - p[0])))
    p_prime_2 = np.linalg.lstsq(X[:, 0:3], term, rcond=-1)
    p_prime_2 = p_prime_2[0]
    # p_prime_2 = np.dot(np.linalg.pinv(X[:, 0:3]), term)
    R_x = 1/p[0]
    R_y = 1/p[1]
    Q_x = R_x**2*(p_prime_2[0] - 0.5*p_prime_2[2]*np.tan(p[2])) - 1
    Q_y = R_y**2*(p_prime_2[1] - 0.5*p_prime_2[2]*np.tan(p[2])) - 1
    phi = p[2]
    return R_x, R_y, phi, Q_x, Q_y

def f_biconic_model(init, *data):
    """biconical model; inital guess: init=[a',b',d',u',v',w',mz], data to fit to: data= [x_i,y_i,z_i]"""
    data = data[0]
    c = (init[3]*data[0, :]**2 + init[4]*data[1, :]**2 + init[5]*data[0, :]*data[1, :])/(init[0]*data[0, :]**2 + init[1]*data[1, :]**2 + init[2]*data[0, :]*data[1, :])
    return np.sum(( init[0]*data[0, :]**2 + init[1]*data[1, :]**2 + init[2]*data[0, :]*data[1, :] + c*(data[2, :] - init[6])**2 - 2*(data[2, :] - init[6]) )**2)


def f2_biconic_model(init, *data):
    data = data[0]
    x = data[:, 0]
    y = data[:, 1]
    z = data[:, 2]
    return np.sum((-z + init[4] + (x**2/init[0] + y**2/init[1])/(1 + np.sqrt(1 - (1+init[2])*x**2/init[0]**2 - (1+init[3])*y**2/init[1]**2)))**2)

def nm_biconic_fit(data):
    x = np.reshape(data[:, 0], [len(data[:, 0]), 1])
    y = np.reshape(data[:, 1], [len(data[:, 0]), 1])
    z = np.reshape(data[:, 2], [len(data[:, 0]), 1])
    init = np.array([1/7.6, 1/7.6, 0, 0, 0, 0, 0])
    res = optimize.minimize(f_biconic_model, init, np.array([x, y, z]), method='Nelder-Mead', options={'xtol': 1e-8})
    p_prime = res.x
    a_1 = 0.5 * (-(-p_prime[0] - p_prime[1]) + np.sqrt((-p_prime[0] - p_prime[1])**2 - 4*(p_prime[0]*p_prime[1] - p_prime[2]**2/4) + 0j))
    a_2 = 0.5 * (-(-p_prime[0] - p_prime[1]) - np.sqrt((-p_prime[0] - p_prime[1])**2 - 4*(p_prime[0]*p_prime[1] - p_prime[2]**2/4) + 0j))
    a_1 = np.round(a_1, decimals=5)
    a_2 = np.round(a_2, decimals=5)
    p = np.zeros([5,1])
    if a_1 > 0 and (p_prime[0] - a_1) / (p_prime[0] + p_prime[1] - 2 * a_1) >= 0:
        p[0] = np.real(a_1)
    elif a_2 > 0 and (p_prime[0] - a_2) / (p_prime[0] + p_prime[1] - 2 * a_2) >= 0:
        p[0] = np.real(a_2)
    else:
        p[0] = np.inf
    p[1] = -p[0] + (p_prime[0] + p_prime[1])
    if p[0] == p[1]:
        p[2] = 0
    else:
        p[2] = 0.5 * (np.arcsin(p_prime[2] / (p[1] - p[0])))
    R_x = 1 / p[0]
    R_y = 1 / p[1]
    Q_x = R_x**2*(p_prime[3] - 0.5*p_prime[5] * np.tan(p[2])) - 1
    Q_y = R_y**2*(p_prime[4] - 0.5*p_prime[5] * np.tan(p[2])) - 1
    phi = p[2]
    return R_x, R_y, phi, Q_x, Q_y, p_prime[6]

def f_sphere(init, *data):
    data = np.array(data[0:3])[:, :, 0]
    x = data[0, :]
    y = data[1, :]
    z = data[2, :]
    return (-init[0]**2 + x**2 + y**2 + (z-init[1])**2)**2

def sphere_fit(data):
    x = np.reshape(data[:, 0], [len(data[:, 0]), 1])
    y = np.reshape(data[:, 1], [len(data[:, 0]), 1])
    z = np.reshape(data[:, 2], [len(data[:, 0]), 1])
    init = np.array([7.6, 0])
    res = optimize.least_squares(f_sphere, init, args=np.array([x, y, z]))
    return res.x