import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import fsolve

nodes_pos_file = {'fine': 'anterior_surface_node_pos_fine.dat', 'coarse_1': 'anterior_surface_node_pos_coarse.dat', 'coarse_2': 'anterior_surface_node_pos_coarse_2.dat', 'w/o pore pres': 'anterior_surface_node_pos_without_pore_pres.dat'}
nodes_id_file = {'fine': 'anterior_surface_node_id_fine.dat', 'coarse_1': 'anterior_surface_node_id_coarse.dat', 'coarse_2': 'anterior_surface_node_id_coarse_2.dat', 'w/o pore pres': 'anterior_surface_node_id_without_pore_pres.dat'}
nodes_disp_file = {'fine': 'anterior_surface_displacement_fine.dat', 'coarse_1': 'anterior_surface_displacement_coarse.dat', 'coarse_2': 'anterior_surface_displacement_coarse_2.dat', 'w/o pore pres': 'anterior_surface_displacement_without_pore_pres.dat'}
plt.figure()
for f_num in nodes_disp_file:

    # load position of nodes
    nodes = []
    with open(nodes_pos_file[f_num]) as fh:
        next(fh)
        for line in fh:
            id_1 = line.find("<node id=")
            id_2 = line.find("> ")
            id_3 = line.find("</node>")
            nodes.append([int(line[id_1+10:id_2-1]), float(line[id_2+3:id_2+16]), float(line[id_2+19:id_2+32]), float(line[id_2+35:id_3])])

    # load set definition "id of nodes" or "indexes of set"
    nodes_index = []
    with open(nodes_id_file[f_num]) as fh:
        next(fh)
        for line in fh:
            id_1 = line.find("<node id=")
            id_2 = line.find("/>")
            nodes_index.append(int(line[id_1 + 10:id_2 - 1]))

    # load displacement of nodes
    nodes_disp = []
    t = []
    with open(nodes_disp_file[f_num]) as fh:
        for line in fh:
            if line.find('*Step') == 0:
                line = next(fh)
                id_1 = line.find('=')
                t.append(float(line[id_1+1:-1]))
                line = next(fh)
                line = next(fh)
            nodes_disp.append([float(line.split(',')[0]), float(line.split(',')[1]), float(line.split(',')[2]), float(line.split(',')[3])])

    nodes = np.asarray(nodes)
    nodes_index = np.asarray(nodes_index)
    nodes_disp = np.reshape(np.asarray(nodes_disp), (len(t), len(nodes_index)*4), order='C')
    t = np.asarray(t)
    x = np.zeros((len(nodes_index), 3*len(t)))
    iii = 0

    # stitch disp and pos together
    for steps in t:
        ii = 0
        for i in nodes_index:
            temp = nodes[np.where(nodes[:, 0] == i)[0], :] + nodes_disp[iii, int(np.where(nodes_disp[iii, :] == i)[0]): int(np.where(nodes_disp[iii, :] == i)[0])+4]
            x[ii, iii*3:(iii+1)*3] = temp[0, 1:]
            ii += 1
        iii += 1
    x = np.array(sorted(x, key=lambda x_column: x_column[0]))

    # calculate Radius and power of the eye
    n = 1.3375
    R_n = np.zeros(np.array([len(t), 1]))
    power_eye = np.zeros(np.array([len(t), 1]))

    def radiusfun(circle, *pos):
        pos = np.reshape(np.asarray(pos), [3, 2])
        x_ = pos[:, 0]
        y = pos[:, 1]
        r_ = circle[0]
        m_x = 0                 # circle[1]
        m_y = circle[1]
        f = np.empty(2)
        f[0] = (x_[0]-m_x)**2 + (y[0]-m_y)**2 - r_**2
        f[1] = (x_[2]-m_x)**2 + (y[2]-m_y)**2 - r_**2
        # f[2] = (x_[2]-m_x)**2 + (y[2]-m_y)**2 - r_**2
        return f

    for j in range(len(t)):
        pos_ = np.zeros([3,2])
        pos_[:, 0] = np.transpose([x[0, j*3], x[(np.abs(x[:, 0] - 1.5)).argmin(), j*3], x[(np.abs(x[:, 0] - 3)).argmin(), j*3]])
        pos_[:, 1] = np.transpose([x[0, j*3+1], x[(np.abs(x[:, 0] - 1.5)).argmin(), j*3+1], x[(np.abs(x[:, 0] - 3)).argmin(), j*3+1]])
        r = fsolve(radiusfun, np.array([7.6, 0]), args=pos_)
        R_n[j] = r[0]
        power_eye[j] = (n-1)/(R_n[j]*1e-3)

    plt.plot(t/3600, R_n, label=f_num)
    plt.legend()
    plt.xlabel('time [h]')
    plt.ylabel('radius [mm]')
    plt.xticks((np.arange(0, 22, 2)))
plt.show()